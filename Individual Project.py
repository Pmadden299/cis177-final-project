'''
Patrick Madden
CIS177 Individual Project
Due 7/31/2016
'''

from random import shuffle # Import shuffle to shuffle the deck

class StandardDeck: #
    # The following are static variables of the StandardDeck class
    suits = ["Spades", "Diamonds", "Clubs", "Hearts"]
    ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10","Jack", "Queen", "King"]
    size = 52

#-----------------------------------------------------------------------

class Card:
    def __init__(self, value): # Initializer (constructor)
        self.__value = value

    def __str__(self): # Return the string representation of this card
        suit = self.__value // 13 # integer from 0 to 3
        rank = self.__value % 13 # integer from 0 to 12
        string = StandardDeck.ranks[rank] + " of " + StandardDeck.suits[suit]
        return string

    def get_card_int(self):# Return the integer value of this card (Accessor method for __value)
        return self.__value

    def get_card_suit(self): # Function I created to return the suit of a specific card
        cardSuit = self.__value // 13  # integer from 0 to 3
        return cardSuit # Had to return this way, when tried otherwise I was getting Attribute errors

    def get_card_rank(self): # Function I created to return the rank of a specific card
        cardRank = self.__value % 13  # integer from 0 to 12
        return cardRank # Had to return this way, when tried otherwise I was getting Attribute errors

#-----------------------------------------------------------------------

def deckShuffle(): # Shuffle function that creates a new deck that is completely shuffled
    deck = list(range(0, StandardDeck.size)) # Creates a variable (deck) that is a list of the 52 cards in the deck
    print("Shuffling the deck...") # Prints out a message telling the user that the deck is being shuffled
    shuffle(deck) # runs the shuffle function
    return deck # returns the shuffled deck

def deckScore(deck): # Function that counts up the assigned values of the cards
    deckTotal = 0 # Sets the initial deck value to 0 in case the function was run previously

    for i in deck: # For loop to go through every position in the deck
        card = Card(i) # creates a variable (card) to act as Card(i), (Did it this way to avoid more type errors
        cardRankVal = card.get_card_rank() # Created variable that makes the get rank stuff if and elif statements easier to read
        cardSuitVal = card.get_card_suit() # Same as the previous line but with suit instead of rank

        if(int(cardRankVal) > 0 and int(cardRankVal) < 10): # If the card is a num card runs this
            if(int(cardSuitVal) == 1 or int(cardSuitVal) == 3): # If it's a red card, runs this if
                cardScore = -(int(cardRankVal+1)) # Creates var cardScore that is assigned a negative value
                deckTotal += cardScore # Adds the card score to the total score
            else: # If it's a black card, runs this
                cardScore = int(cardRankVal+1) # Creates var cardScore that is assigned a value
                deckTotal += cardScore # Adds the card score to the total score
        elif(int(cardRankVal) == 0): # rank of 0 is an Ace, runs this
            deckTotal = 0 # Sets the deck score to 0
        elif(int(cardRankVal) == 10): # rank of 10 is a Jack, runs this
            i = i+1 # Skips the next card
        elif(int(cardRankVal) == 11): # rank of 11 is a Queen, runs this
            if(int(cardSuitVal) == 1 or int(cardSuitVal) == 3): # Checks if the suit is red
                cardScore = -(Card(i-1).get_card_rank()) # Subtracts the previous card
                deckTotal += cardScore # Adds the score to the total
            else: # If it isn't red, it's black suit
                cardScore = Card(i-1).get_card_rank() # Adds previous card
                deckTotal += cardScore # Adds score to the total
        elif(int(cardRankVal) == 12): # rank of 12 is a King, runs this
            deckTotal += 15 # Adds 15 to the total score

    return deckTotal # Returns the deck total

#-----------------------------------------------------------------------

def playerGuess(guess, score): # Function for points based on the guess

    if(int(guess) == int(score)): # Checks the guess is equal to the score
        return 5 # If true returns 5 points
    elif(int(guess) <= (int(score)+10) and int(guess) >= (int(score)-10)): # If it within the 10 range this if is run
        return 2 # If true returns 2 points
    elif(int(guess) <= (int(score)+25) and int(guess) >= (int(score)-25)): # If not in 10 range but in 25 range, runs this
        return 1 # Returns 1 point
    elif(int(guess) > (int(score)+40) or int(guess) < (int(score)-40)): # If more than 40 away, runs this
        return -2 # Returns -2 points
    else: # If it isn't covered by all the others runs this
        return 0 # returns 0 points

#-----------------------------------------------------------------------
#Start of the Main function
print("This is my Fizbin game. Here are the rules:\n"
      "Fizbin is a two player game. Each card in the deck has a specific value assigned to it based on what suit and "
      "rank it has. This will be explained later.\nEach round the deck will be shuffled and both players guess "
      "what the point total is for the deck. Players get 1 point if they are within 25 of the score,\nTwo points if they "
      "are within 10 of the score, five points if you get the number exactly, and you lose two points if you are more "
      "than 40 away.\nThe players repeat this until one gets to 15 (or more) points. At that point, the player who got "
      "above that wins the game.\n\n"
      "Card Values:\n"
      "Black Cards (Spades and Clubs): These cards are positive.\n"
      "Red Cards (Hearts and Diamonds): These cards are negative.\n"
      "Aces: Set the total back to 0.\n"
      "Numeric cards (2-10): Add/subtract from the total based on the card number and suit.\n"
      "Jacks: Causes the next card to be skipped (this one also has a value of 0).\n"
      "Queens: Add/subtract the value of the previous card.\n"
      "Kings: Add 15 (even if it is a red card).\n")
# block of text explaining the game + rules

gameStart = input('Press <ENTER> to continue...')  # Just used this to let the player start the game
player1Score = 0
player2Score = 0
roundNum = 1

# Sets the score for both players to 0 and round number to 1

while(player1Score < 15 or player2Score < 15): # While the players have less than 15 points, this while loop runs
    print("\nRound " + str(roundNum) + ":") # Prints out the round num
    shuffled = deckShuffle() # Shuffles the deck using deckShuffled and assigns the deck to variable shuffled
    deckValue = deckScore(shuffled) # Gets the value of the deck by using deckScore with the shuffled deck assigns to var

    while True: # Start while-loop and run the code blocks below.
        try:  # try the code blocks below, if it produces an error then proceed to the except block
            p1Guess = input("Player 1 input your guess:")
            p2Guess = input("Player 2 input your guess:")

            # Gets the queses from the players assigns the nums to variables
            player1Score += playerGuess(p1Guess, deckValue)
            player2Score += playerGuess(p2Guess, deckValue)

            break  # if no error is found so far, then break out of the loop

        except Exception:  # This code runs only when there's an error under try
            print("\nENTER INTEGER ONLY!\nTRY AGAIN\n")

            # after print, go back to try

    # Adds the score of both players using the playerGuess function for each guess
    print("\nThe value of the deck this round was:",deckValue,
    "\nPlayer 1 was",abs(int(deckValue)-int(p1Guess)),"off from the value and earned",playerGuess(p1Guess,deckValue),"points "
    "this round\nPlayer 2 was",abs(int(deckValue)-int(p2Guess)),"off from the value and earned",playerGuess(p2Guess,deckValue),
    "points this round")
    # This block prints out the stats from this round (deckValue, how many points received)

    print("\nPlayer 1 has", player1Score, "points...")
    print("Player 2 has", player2Score, "points...")
    # This block prints out the total score of each player

    roundNum += 1 # Adds 1 to the round number
    if(player1Score > 15 or player2Score > 15): # If a player is over 15 points runs this as a fail-safe for the while loop(had issues with it earlier)
        break # Ends the while loop

print("\n\nGame Over!","\nPlayer 1 had",player1Score,"points","\nPlayer 2 had",player2Score,"points")
# prints out the game over message, tells each player their score