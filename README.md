CIS177 Final Project README file

This is my Fizbin game. Here are the rules:

Fizbin is a two player game. Each card in the deck has a specific value assigned to it based on what suit and rank it has. This will be explained later. Each round the deck will be shuffled and both players guess what the point total is for the deck. Players get 1 point if they are within 25 of the score, two points if they are within 10 of the score, five points if you get the number exactly, and you lose two points if you are more than 40 away. The players repeat this until one gets to 15 (or more) points. At that point, the player who got above that wins the game.

Card Values:
Black Cards (Spades and Clubs): These cards are positive.
Red Cards (Hearts and Diamonds): These cards are negative.
Aces: Set the total back to 0.
Numeric cards (2-10): Add/subtract from the total based on the card number and suit.
Jacks: Causes the next card to be skipped (this one also has a value of 0).
Queens: Add/subtract the value of the previous card.
Kings: Add 15 (even if it is a red card).

Issues I ran into:

-----------------------------------------------------------------------------------------------
AttributeError: 'int' object has no attribute '_Card__Value'

-This started happening when I added a function to the Card class and wanted to return the    specific values for the suit and the rank. Couldn't figure out exactly why this happened, but I found a work around for it.

Changed to this and it worked...

(In the Card Class)
    def get_card_suit(self):
        cardSuit = self.__value // 13
        return cardSuit

    def get_card_rank(self):
        cardRank = self.__value % 13
        return cardRank

(Later in a different Function)

    card = Card(i)
    cardRankVal = card.get_card_rank()
    cardSuitVal = card.get_card_suit()


----------------------------------------------------------------------------------------------
During the course of working on the game I wanted the deck to be split into different parts and make it a lot more random what cards could possibly be affecting the outcome of the final deckvalues. I ran into some issues with the function def deckScore(deck): because I know how to split the deck into multiple parts without getting the same deckvalue every time I ran the game. I eventually decided to have both players just guess on the whole deck to make it less complicated.
