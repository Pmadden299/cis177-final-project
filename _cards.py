"""
Tuesday, August 9, 2016
"""


class CardsProperty:

    ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10","Jack", "Queen", "King"]
    suits = ["Spades", "Hearts", "Diamonds", "Clubs"]

    size = len(suits) * len(ranks)


class Card:

    def __init__(self, value=0):
        self.__value = value

    def __str__(self):  # Return the string representation of this card
        rank = self.__value % 13  # equals to integer ranging from 0 to 12
        suit = self.__value // 13  # equals to integer ranging from 0 to 3

        return str( CardsProperty.ranks[rank] + "-of-" + CardsProperty.suits[suit] )

    def get_card_int(self):# Return the integer value of this card (Accessor method for __value)
        return self.__value

    def get_card_suit(self): # Function I created to return the suit of a specific card
        cardSuit = self.__value // 13  # integer from 0 to 3
        return cardSuit # Had to return this way, when tried otherwise I was getting Attribute errors

    def get_card_rank(self): # Function I created to return the rank of a specific card
        cardRank = self.__value % 13  # integer from 0 to 12
        return cardRank # Had to return this way, when tried otherwise I was getting Attribute errors

class DeckOfCardsString:

    deck = []

    for i in range(CardsProperty.size):
        deck.append( Card(i).__str__() )

