from tkinter import *
from random import randint
from random import shuffle
from _cards import *
from _player import Player


class FizbinGUI:

    window = Tk()
    NCARDS = 4

    CARDSSTRINGLIST = DeckOfCardsString.deck  # store strings of cards' names

    CARDSPHOTOIMGAES = []  # store tkinter PhotoImages
    for i in range(52):
        CARDSPHOTOIMGAES.append( PhotoImage(file="cards-gif/" + str(i + 1) + ".gif") )

    """
    Initializer
    ------------------------------------------------------------
    """
    def __init__(self):
        # Instance variables
        self.Player1 = Player(name="Player1", score=0, turn=True)
        self.Player2 = Player("Player2", 0, False)
        self.isGameRunning = True
        self.__startGUI()

    """
    Defining GUI
    ------------------------------------------------------------
    """
    def __DisplayPlayersScore(self, player, window):
        container = Frame(window)
        # create widgets in container
        playersName = Label(container,
                            text=player.getPlayersName(),
                            font=("Helvetica", 28)
                            )
        self.playersScore = Label(container,
                                  text=player.getPlayersScore(),
                                  font=("Helvetica", 24),
                                  fg="green",
                                  bg="black"
                                  )
        self.turn = Label(container, text="TURN: " + str(player.isTurn))
        # pack widgets
        playersName.pack()
        self.playersScore.pack()
        self.turn.pack()
        # attach container to window
        container.pack(side=LEFT)

    def __DisplayInstruction(self, window=Tk):
        self.instruction = Label(window,
                                 text=self.getInstruction(),
                                 bg="black",
                                 fg="green",
                                 border=15
                                 )
        self.instruction.pack()
        print("")
    def __DisplayPlayersGUI(self, name="Player", window=Tk):
        container = Frame(window, border=30)  # create a container to holds widgets
        # create widgets in container
        self.playersName = Label(container, text=name, font=("Helvetica", 24))
        self.cards = []  # store random Labels of cards
        for i in range(self.NCARDS):
            cardLabel = Label(container, image=self.CARDSPHOTOIMGAES[randint(1,51)])
            cardLabel.bind("<Button-1>", self.testEvent)
            self.cards.append(cardLabel)
        self.guessLabel = Label(container, text="GUESS: ")
        self.guessEntry = Entry(container, width=4)
        self.enterBtn = Button(container, text="ENTER", command=self.testButton)
        # attach widgets to container
        self.playersName.grid(row=0, column=0, columnspan=len(self.cards))
        for i in range(self.NCARDS):
            self.cards[i].grid(row=1, column=i)
        self.guessLabel.grid(row=2, column=0)
        self.guessEntry.grid(row=2, column=1)
        self.enterBtn.grid(row=2, column=2)
        # attach container to window
        container.pack()

    def __startGUI(self):
        # create box score
        boxScore = Frame(self.window)
        self.__DisplayPlayersScore(player=self.Player1, window=boxScore)
        self.__DisplayPlayersScore(player=self.Player2, window=boxScore)
        boxScore.pack()
        self.__DisplayInstruction(window=self.window)
        self.__DisplayPlayersGUI(name=self.Player1.getPlayersName(), window=self.window)
        self.__DisplayPlayersGUI(name=self.Player2.getPlayersName(), window=self.window)

    """
    Defining GUI Functions
    --------------------------------------------------------
    """
    def testEvent(self, event):
        print("event test")

    def testButton(self):
        print("btn test")

    def getInstruction(self):
        message = ["CLICK ON 3 CARDS", "ENTER YOUR GUESS"]

        return message[0]


if __name__ == '__main__':
    program = FizbinGUI()
    mainloop()

def deckShuffle(): # Shuffle function that creates a new deck that is completely shuffled
    deck = FizbinGUI.cards() # Couldn't get this to work :[
    print("Shuffling the deck...") # Prints out a message telling the user that the deck is being shuffled
    shuffle(deck) # runs the shuffle function
    return deck # returns the shuffled deck

def deckScore(deck): # Function that counts up the assigned values of the cards
    deckTotal = 0 # Sets the initial deck value to 0 in case the function was run previously

    for i in deck: # For loop to go through every position in the deck
        card = Card(i) # creates a variable (card) to act as Card(i), (Did it this way to avoid more type errors
        cardRankVal = card.get_card_rank() # Created variable that makes the get rank stuff if and elif statements easier to read
        cardSuitVal = card.get_card_suit() # Same as the previous line but with suit instead of rank

        if(int(cardRankVal) > 0 and int(cardRankVal) < 10): # If the card is a num card runs this
            if(int(cardSuitVal) == 1 or int(cardSuitVal) == 3): # If it's a red card, runs this if
                cardScore = -(int(cardRankVal+1)) # Creates var cardScore that is assigned a negative value
                deckTotal += cardScore # Adds the card score to the total score
            else: # If it's a black card, runs this
                cardScore = int(cardRankVal+1) # Creates var cardScore that is assigned a value
                deckTotal += cardScore # Adds the card score to the total score
        elif(int(cardRankVal) == 0): # rank of 0 is an Ace, runs this
            deckTotal = 0 # Sets the deck score to 0
        elif(int(cardRankVal) == 10): # rank of 10 is a Jack, runs this
            i = i+1 # Skips the next card
        elif(int(cardRankVal) == 11): # rank of 11 is a Queen, runs this
            if(int(cardSuitVal) == 1 or int(cardSuitVal) == 3): # Checks if the suit is red
                cardScore = -(Card(i-1).get_card_rank()) # Subtracts the previous card
                deckTotal += cardScore # Adds the score to the total
            else: # If it isn't red, it's black suit
                cardScore = Card(i-1).get_card_rank() # Adds previous card
                deckTotal += cardScore # Adds score to the total
        elif(int(cardRankVal) == 12): # rank of 12 is a King, runs this
            deckTotal += 15 # Adds 15 to the total score

    return deckTotal # Returns the deck total

#-----------------------------------------------------------------------

def playerGuess(guess, score): # Function for points based on the guess

    if(int(guess) == int(score)): # Checks the guess is equal to the score
        return 5 # If true returns 5 points
    elif(int(guess) <= (int(score)+10) and int(guess) >= (int(score)-10)): # If it within the 10 range this if is run
        return 2 # If true returns 2 points
    elif(int(guess) <= (int(score)+25) and int(guess) >= (int(score)-25)): # If not in 10 range but in 25 range, runs this
        return 1 # Returns 1 point
    elif(int(guess) > (int(score)+40) or int(guess) < (int(score)-40)): # If more than 40 away, runs this
        return -2 # Returns -2 points
    else: # If it isn't covered by all the others runs this
        return 0 # returns 0 points


player1Score = 0
player2Score = 0
roundNum = 1

# Sets the score for both players to 0 and round number to 1

while(player1Score < 15 or player2Score < 15): # While the players have less than 15 points, this while loop runs
    print("\nRound " + str(roundNum) + ":") # Prints out the round num
    shuffled = deckShuffle() # Shuffles the deck using deckShuffled and assigns the deck to variable shuffled
    deckValue = deckScore(shuffled) # Gets the value of the deck by using deckScore with the shuffled deck assigns to var

    while True: # Start while-loop and run the code blocks below.
        try:  # try the code blocks below, if it produces an error then proceed to the except block
            p1Guess = FizbinGUI.guessEntry(1) # Couldn't get this to work :[
            p2Guess = FizbinGUI.guessEntry(2) # Couldn't get this to work :[

            # Gets the queses from the players assigns the nums to variables
            player1Score += playerGuess(p1Guess, deckValue)
            player2Score += playerGuess(p2Guess, deckValue)

            break  # if no error is found so far, then break out of the loop

        except Exception:  # This code runs only when there's an error under try
            print("\nENTER INTEGER ONLY!\nTRY AGAIN\n")

            # after print, go back to try

    # Adds the score of both players using the playerGuess function for each guess
    print("\nThe value of the deck this round was:",deckValue,
    "\nPlayer 1 was",abs(int(deckValue)-int(p1Guess)),"off from the value and earned",playerGuess(p1Guess,deckValue),"points "
    "this round\nPlayer 2 was",abs(int(deckValue)-int(p2Guess)),"off from the value and earned",playerGuess(p2Guess,deckValue),
    "points this round")
    # This block prints out the stats from this round (deckValue, how many points received)

    print("\nPlayer 1 has", player1Score, "points...")
    print("Player 2 has", player2Score, "points...")
    # This block prints out the total score of each player

    roundNum += 1 # Adds 1 to the round number
    if(player1Score > 15 or player2Score > 15): # If a player is over 15 points runs this as a fail-safe for the while loop(had issues with it earlier)
        break # Ends the while loop

print("\n\nGame Over!","\nPlayer 1 had",player1Score,"points","\nPlayer 2 had",player2Score,"points")
# prints out the game over message, tells each player their score
